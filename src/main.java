/**
 * Created by Павел on 23.10.2014.
 */
public class main {
    public static void main(String... args) {
        Dog dog = new Dog();

        Dog dog1 = new Dog("Собака", "гав гав");

        Dog dog2 = new Dog("тузик", "тяф тяф");
        dog.printDisplay();
        dog1.printDisplay();
        dog2.printDisplay();

        Cat cat = new Cat();

        Cat cat1 = new Cat("Кошка", "мяу мяу");

        Cat cat2 = new Cat("Кошак", "мьяук мьяук");
        cat.printDisplay();
        cat1.printDisplay();
        cat2.printDisplay();
    }
};
